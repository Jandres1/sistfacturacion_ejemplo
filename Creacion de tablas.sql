create database SFClaro

restore database SFClaro from 
disk='C:\SFClaro.bak' with replace

use SFClaro

create table Productos(
Id_Prod int identity(1,1) primary key not null,
Marcar nvarchar(35) not null,
Modelo nvarchar(35) not null,
exist int default 1 not null,
precio float not null
)

create rule entpos
as
@v>0

sp_bindrule 'entpos','Productos.exist'

sp_bindrule 'entpos','Productos.precio'

create table Servicios(
Id_Serv int identity(1,1) primary key not null,
Tipo_Serv nvarchar(20) check(Tipo_Serv like '[Tel|Int|Cable|CClaro]'),
descripcionserv nvarchar(35) not null,
costoserv float not null
)

sp_bindrule 'entpos','Servicios.costoserv'

create table Solicitud(
Id_Sol char(4) primary key not null,
Fecha_Sol datetime default getdate() not null,
Id_Serv int foreign key references Servicios(Id_Serv) not null,
Id_cliente int foreign key references Cliente_Natural(Id_cliente)  not null,
NRuc char(11) foreign key references Cliente_Juridico(NRuc) not null
)

-- Se me olvido por eso altere, pero no es necesario
-- alter table Solicitud add Id_cliente int not null 
-- alter table Solicitud add NRuc char(11) not null
-- alter table Solicitud add constraint fk_cn foreign key 
-- (Id_cliente) references Cliente_Natural(Id_cliente)
-- alter table Solicitud add constraint fk_cj foreign key 
-- (NRuc) references Cliente_Juridico(NRuc)

create table Facturas_CN(
NFact int identity(1,1) primary key not null,
Fecha_Fact date default getdate() not null,
Id_cliente int foreign key references Cliente_Natural(Id_cliente) not null,
subtotalf float,
totalf float
)

create rule poscero as @x>=0

sp_bindrule 'poscero','Facturas_CN.subtotalf'
sp_bindrule 'poscero','Facturas_CN.totalf'

create table Detalle_FacturaCN(
NFact int foreign key references Facturas_CN(NFact) not null,
Id_Prod int foreign key references Productos(Id_Prod) not null,
cantf int not null,
subtotalp float,
primary key(NFact,Id_Prod) 
)

sp_bindrule 'entpos','Detalle_FacturaCN.cantf'
sp_bindrule 'entpos','Detalle_FacturaCN.subtotalp'

create table Contratos(
NContrato char(4) check (NContrato like '[N|J][0-9][0-9][0-9]') primary key not null,
Fecha_Contrato date default getdate() not null,
Vigencia int default 1 not null,
Id_Sol char(4) foreign key references Solicitud(Id_Sol) not null
)

create table Detalle_Contratos(
NContrato char(4) foreign key references Contratos(NContrato) not null,
Id_Serv int foreign key references Servicios(Id_Serv) not null,
cants int not null,
primary key(NContrato,Id_Serv)
)

sp_bindrule 'entpos','Contratos.Vigencia'
sp_bindrule 'entpos','Detalle_Contratos.cants'

create table Subcontratados(
Id_Subcont int identity(1,1) primary key not null,
Nombre_Subcont nvarchar(35) not null,
Tel_Cont char(8) check(Tel_Cont like '[2|5|7|8][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') not null,
Dir_Cont varchar(70) not null
)

create table Orden_Servicio(
NOrdenServ char(4) primary key not null,
NContrato char(4) foreign key references Contratos(NContrato) not null,
Id_Subcont int foreign key references Subcontratados(Id_Subcont) not null,
)

create table Marcas(
Id_Marca int identity(1,1) primary key not null,
DescMarca nvarchar(50) not null
)

select * from Productos

alter table Productos drop column Marca
alter table Productos add Id_Marca int not null
alter table Productos add constraint fk_mp foreign key (Id_Marca)
references Marcas(Id_Marca)

create table Factura_CJ(
NFactCJ int identity(1,1) primary key not null,
Fecha_Fact date default getdate() not null,
NRuc char(11) foreign key references Cliente_Juridico(NRuc) not null,
Fecha_Venc date,
descuento float not null,
subtotalf money,
totalf money
)

create table Detalle_FacturaCJ(
NFactCJ int foreign key references Factura_CJ(NFactCJ) not null,
Id_Prod int foreign key references Productos(Id_Prod) not null,
cantf int not null,
subtotalp float,
primary key(NFactCJ,Id_Prod)
)

sp_bindrule 'entpos','Factura_CJ.descuento'

sp_bindrule 'entpos','Factura_CJ.subtotalf'

sp_bindrule 'entpos','Factura_CJ.totalf'

sp_bindrule 'entpos','Detalle_FacturaCJ.cantf'

sp_bindrule 'entpos','Detalle_FacturaCJ.subtotalp'

backup database SFClaro to disk='C:\Program Files\Microsoft SQL Server\MSSQL12.SQLSERVER2014\MSSQL\Backup\SFClaro.bak'