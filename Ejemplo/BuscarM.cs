﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Ejemplo
{
    public partial class BuscarM : Form
    {
        SqlConnection con;
        SqlCommand command;
        SqlDataReader reader;
        public BuscarM()
        {
            InitializeComponent();
        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text == " ")
                {
                    MessageBox.Show("No puede estar en blanco");
                }
                else
                {
                    string sql = "Select * from Marcas where Id_Marca = " + textBox1.Text;
                    con = new SqlConnection("Server = DESKTOP-0NJ9CU0; Database =  SFClaro; UID = Andres; PWD = 1234");
                    command = new SqlCommand("BMarca", con);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ID", textBox1.Text);
                    con.Open();
                    command.ExecuteNonQuery();
                    reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            textBox2.Text = reader.GetString(1);
                        }
                    }
                    con.Close();
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show("Problemas al buscar" + ex.ToString());
            }
        }
    }
}
