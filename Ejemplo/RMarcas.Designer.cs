﻿namespace Ejemplo
{
    partial class RMarcas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.MarcasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SFClaroData = new Ejemplo.SFClaroData();
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.button1 = new System.Windows.Forms.Button();
            this.MarcasTableAdapter = new Ejemplo.SFClaroDataTableAdapters.MarcasTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.MarcasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SFClaroData)).BeginInit();
            this.SuspendLayout();
            // 
            // MarcasBindingSource
            // 
            this.MarcasBindingSource.DataMember = "Marcas";
            this.MarcasBindingSource.DataSource = this.SFClaroData;
            // 
            // SFClaroData
            // 
            this.SFClaroData.DataSetName = "SFClaroData";
            this.SFClaroData.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer2
            // 
            reportDataSource1.Name = "MarcasData";
            reportDataSource1.Value = this.MarcasBindingSource;
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "Ejemplo.ReporteM.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(74, 67);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.Size = new System.Drawing.Size(396, 246);
            this.reportViewer2.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(222, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Generar Reporte ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MarcasTableAdapter
            // 
            this.MarcasTableAdapter.ClearBeforeFill = true;
            // 
            // RMarcas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 325);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.reportViewer2);
            this.Name = "RMarcas";
            this.Text = "RMarcas";
            this.Load += new System.EventHandler(this.RMarcas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MarcasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SFClaroData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer2;
        private System.Windows.Forms.BindingSource MarcasBindingSource;
        private SFClaroData SFClaroData;
        private System.Windows.Forms.Button button1;
        private SFClaroDataTableAdapters.MarcasTableAdapter MarcasTableAdapter;
    }
}