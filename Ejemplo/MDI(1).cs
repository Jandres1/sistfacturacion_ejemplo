﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo
{
    public partial class MDI : Form
    {
        Marcas m = new Marcas();
        public MDI()
        {
            InitializeComponent();
        }

        private void nuevaMarcaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m.MdiParent = this;
            m.Show();
        }

        private void listarMarcasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LMarcas lm = new LMarcas();
            lm.MdiParent = this;
            lm.Show();
        }
    }
}
