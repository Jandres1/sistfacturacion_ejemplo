﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo
{
    public partial class LMarcas : Form
    {
        public LMarcas()
        {
            InitializeComponent();
        }

        private void LMarcas_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'sFClaroData.Marcas' Puede moverla o quitarla según sea necesario.
            this.marcasTableAdapter.Fill(this.sFClaroData.Marcas);

        }
    }
}
