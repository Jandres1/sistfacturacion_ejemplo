﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo
{
    public partial class MDI : Form
    {
        Marcas m = new Marcas();
        public MDI()
        {
            InitializeComponent();
        }

        private void nuevaMarcaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m.MdiParent = this;
            m.Show();
        }

        private void listarMarcasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LMarcas lm = new LMarcas();
            lm.MdiParent = this;
            lm.Show();
        }

        private void modificarMarcasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModMarca Mm = new ModMarca();
            Mm.MdiParent = this;
            Mm.Show();
        }

        private void buscarMarcasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarM BM = new BuscarM();
            BM.MdiParent = this;
            BM.Show();
        }

        private void reporteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RMarcas rm = new RMarcas();
            rm.MdiParent = this;
            rm.Show();
        }
    }
}
