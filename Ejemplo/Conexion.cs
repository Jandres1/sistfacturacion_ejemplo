﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;


namespace Ejemplo
{
    class Conexion
    {
        public SqlConnection conectc = new SqlConnection();

        public Conexion() { }
        public Conexion(string login, string passwd,string Server)
        {
            try
            {

                conectc = new SqlConnection("Server="+Server+";Database=SFClaro;UID=" + login + ";PWD=" + passwd);

                conectc.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de Coneccion", "Sistema de Ejemplo",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            }

        }

    }
}
