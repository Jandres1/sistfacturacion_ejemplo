﻿using System;

using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Ejemplo
{
    public partial class Marcas : Form
    {
        SqlConnection con;
        SqlCommand command;
        
        public Marcas()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if(textBox1.Text==" ")
                {
                    MessageBox.Show("No puede estar en blanco");
                }
                else
                {
                    con = new SqlConnection("Server = DESKTOP-0NJ9CU0; Database =  SFClaro; UID = Andres; PWD = 1234");
                    command = new SqlCommand("NMarca", con);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@DM", textBox1.Text);
                    con.Open();
                    command.ExecuteNonQuery();
                    MessageBox.Show("Registro insertado exitosamente");
                    con.Close();
                    textBox1.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problemas al Insertar");
            }
    }

        private void Marcas_Load(object sender, EventArgs e)
        {

        }
    }
}
