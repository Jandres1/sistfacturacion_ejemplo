﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Threading;

namespace Ejemplo
{
    public partial class Form1 : Form
    {
        Conexion con;

        MDI m = new MDI();
        int contador = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Aceptar_Click(object sender, EventArgs e)
        {
           
            con = new Conexion(textBox1.Text, textBox2.Text);

            if (this.con.conectc.State == ConnectionState.Open)
            {

                m.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Error");
                contador++;

            }

            if (contador > 0 && contador < 3)
            {
                MessageBox.Show("Intento fallido numero " + contador + " le quedan " + (3 - contador) + " intentos");
            }
            else
            {
                if (contador == 3)
            {

                ThreadStart h1 = new ThreadStart(bloquear1);
                Thread hilo1 = new Thread(h1);
                hilo1.Start();
                bloquear1();

                ThreadStart h2 = new ThreadStart(bloquear2);
                Thread hilo2 = new Thread(h2);
                hilo2.Start();
                bloquear2();

            }
            }

        }
        private void bloquear1()
        {
            textBox1.Enabled = false;
        }

        private void bloquear2()
        {
            textBox2.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
