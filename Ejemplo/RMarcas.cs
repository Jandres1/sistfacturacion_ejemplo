﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace Ejemplo
{
    public partial class RMarcas : Form
    {
        public RMarcas()
        {
            InitializeComponent();
        }

        private void RMarcas_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'SFClaroData.Marcas' Puede moverla o quitarla según sea necesario.
            this.MarcasTableAdapter.Fill(this.SFClaroData.Marcas);
            // TODO: esta línea de código carga datos en la tabla 'SFClaroData.Marcas' Puede moverla o quitarla según sea necesario.

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.MarcasTableAdapter.Fill(this.SFClaroData.Marcas);
            this.reportViewer2.RefreshReport();
        }
    }
}
