
use SFClaro

create table Deptos
(
Id_Dpto int identity(1,1) primary key not null,
NombreDpto nvarchar(25) not null
)

create table Municipios(
Id_Mun int identity(1,1) primary key not null,
NombreMun nvarchar(25) not null,
Id_Dpto int foreign key references Deptos(Id_Dpto) not null,
)

create table Comarcas(
Id_Com int identity(1,1) primary key not null,
NombreComarcar nvarchar(25) not null,
Id_Mun int foreign key references Municipios(Id_Mun) not null
)

alter table Cliente_Natural drop column Nombres
alter table Cliente_Natural drop column Apellidos

alter table Cliente_Natural add PNombre 
nvarchar(15) not null

alter table Cliente_Natural add SNombre 
nvarchar(15) 

alter table Cliente_Natural add PApellido 
nvarchar(15) not null

alter table Cliente_Natural add SApellido
nvarchar(15)
 
alter table Cliente_Natural add 
Id_Com int not null

alter table Cliente_Natural add constraint 
fk_cnc foreign key(Id_Com) references 
Comarcas(Id_Com)

-- Creando Procedimiento almacenado de Insercion
create procedure NuevoDpto
@ND nvarchar(25)
as
if(@ND='')
begin
  print 'No puede estar en blanco'
end
else
begin
  insert into Deptos values(@ND)
end

NuevoDpto 'Managua'
NuevoDpto 'Masaya'
NuevoDpto 'Granada'
NuevoDpto 'Leon'

select * from Deptos

-- Procedimiento de Modificacion
create procedure ModD
@ID int,
@ND nvarchar(25)
as
declare @iddepto as int
set @iddepto=(select Id_Dpto from Deptos where
Id_Dpto=@ID)
if(@iddepto=@ID)
begin
   if(@ND='')
   begin
      print 'No puede estar en blanco'
   end
   else
   begin
      update Deptos set NombreDpto=@ND where
	  Id_Dpto=@ID
   end
end
else
begin
  print 'Departamento no registrado'
end

ModD 4,'Rivas'

select * from Deptos where Id_Dpto=4

-- Procedimiento de Busqueda
create procedure BuscarD
@ID int
as
declare @iddpto as int
set @iddpto=(select Id_Dpto from Deptos where
Id_Dpto=@ID)
if(@ID=@iddpto)
begin
  select * from Deptos where Id_Dpto=@ID
end
else
begin
  print 'No se localizo el departamento, no esta registrado'
end

BuscarD 2

-- Procedimiento de Lista
create procedure ListarD
as
select * from Deptos

ListarD

-- Procedimiento de Eliminacion. Con fines academicos
-- Los registros no se eliminan, solo cambian
-- de estado.

create procedure EliminarD
@ID int
as
declare @idd as int
set @idd=(select Id_Dpto from Deptos where
Id_Dpto=@ID)
if(@idd=@ID)
begin
  delete from Deptos where Id_Dpto=@ID
end
else
begin
  print 'Lo sentimos, no podemos eliminar algo no existente'
end

EliminarD 4

ListarD